package validation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class SaleServiceErrorAdvice extends ResponseEntityExceptionHandler {

    @ResponseBody
    @ExceptionHandler({NotEnoughStockException.class})
    public ResponseEntity<String> handleNotEnoughStockException(NotEnoughStockException ex) {

        return new ResponseEntity<>(ex.getMessage(), HttpStatus.OK);
    }

    @ResponseBody
    @ExceptionHandler({NotEnoughMoneyException.class})
    public ResponseEntity<String> handleNotEnoughMoneyException(NotEnoughMoneyException ex) {

        return new ResponseEntity<>(ex.getMessage(), HttpStatus.OK);
    }


    @ResponseBody
    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<String> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {

        return new ResponseEntity<>(ex.getMessage(), HttpStatus.OK);
    }

}
