package service;

import dao.ProductDao;
import model.Product;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Service
public class SaleService {

    private ProductDao productDao;

    public SaleService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public List<Product> generatePurchasableProducts(List<String> productCodes) {
        List<Product> products = new ArrayList<>();
        for (String productCode : productCodes) {
            Product product = productDao.findProductByCode(productCode);
            products.add(product);
        }
        return products;
    }

    public boolean hasEnoughStockForPurchasableProducts(List<Product> products) {
        boolean hasEnoughStock = true;
        for (Product product : products) {
            if(!product.hasEnoughStock()) {
                hasEnoughStock = false;
            }
        }
        return hasEnoughStock;
    }

    public BigDecimal calculatePayableAmount(List<Product>products) {
        BigDecimal amount = new BigDecimal(0);

        for(Product product : products) {
            amount = amount.add(product.getPrice());
        }

        return amount.setScale(2, RoundingMode.HALF_UP);
    }

    public boolean hasEnoughMoney(BigDecimal payableAmount, BigDecimal paidAmount) {
        return paidAmount.compareTo(payableAmount) >= 0;
    }

    public BigDecimal calculateChangeAmount(List<Product> products, BigDecimal paidAmount) {
        BigDecimal payableAmount = calculatePayableAmount(products);
        BigDecimal changeAmount = paidAmount.subtract(payableAmount);

        return changeAmount.setScale(2, RoundingMode.HALF_UP);
    }

    public void reduceStock(List<Product> products) {
        for (Product product:products) {
            product.setQuantity(product.getQuantity()-1);
            productDao.insertProduct(product);
        }
    }
}
