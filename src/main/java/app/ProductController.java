package app;

import dao.ProductDao;
import model.Product;
import org.springframework.web.bind.annotation.*;
import service.SaleService;
import validation.NotEnoughMoneyException;
import validation.NotEnoughStockException;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class ProductController {

    private ProductDao productDao;
    private SaleService saleService;

    public ProductController(ProductDao productDao, SaleService saleService) {
        this.productDao = productDao;
        this.saleService = saleService;
    }

    @GetMapping("products")
    public List<Product> getAllProducts() {
        return productDao.findAll();
    }

    @GetMapping("sell")
    public BigDecimal getPaymentAmount(@RequestParam List<String> productCodes) {
        if(productCodes.isEmpty()) {
            return new BigDecimal(0);
        }
        List<Product> products = saleService.generatePurchasableProducts(productCodes);

        if (!saleService.hasEnoughStockForPurchasableProducts(products)) {
            throw new NotEnoughStockException("Not enough stock");
        }

        return saleService.calculatePayableAmount(products);
    }

    @GetMapping("finishSale")
    public BigDecimal calculateChangeAmountAndReduceStock(@RequestParam List<String> productCodes, BigDecimal paidAmount) {

        List<Product> products = saleService.generatePurchasableProducts(productCodes);

        if (paidAmount == null
                || !saleService.hasEnoughMoney(saleService.calculatePayableAmount(products), paidAmount)) {
            throw new NotEnoughMoneyException("Not enough money");
        }

        if(!saleService.hasEnoughStockForPurchasableProducts(products)) {
            throw new NotEnoughStockException("Purchase includes products that are not in stock");
        }

        saleService.reduceStock(products);
        return saleService.calculateChangeAmount(products, paidAmount);
    }
}
