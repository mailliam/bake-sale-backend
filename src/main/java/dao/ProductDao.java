package dao;

import model.Product;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ProductDao {

    @PersistenceContext
    private EntityManager em;

    public List<Product> findAll() {
        return em.createQuery("select p from Product p",
                Product.class).getResultList();
    }

    public Product findProductByCode(String productCode) {
        TypedQuery<Product> query = em.createQuery(
                "select p from Product p where p.code = :productCode",
                Product.class
        );

        query.setParameter("productCode", productCode);

        return query.getSingleResult();
    }

    @Transactional
    public Product insertProduct(Product product) {
        if(product.getId() == null) {
            em.persist(product);
        } else {
            em.merge(product);
        }
        return product;
    }
}
