package model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "products")
public class Product {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "code")
    @NonNull
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    @NonNull
    private BigDecimal price;

    @Column(name = "quantity")
    private int quantity;

    public boolean hasEnoughStock() {
        return quantity > 0;
    }

}
