import dao.ProductDao;
import model.Product;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import org.mockito.Mockito;
import service.SaleService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class SaleServiceTest {

    private ProductDao productDao = mock(ProductDao.class);
    private SaleService saleService = new SaleService(productDao);

    @Test
    @DisplayName("Calculates payment amount correctly")
    public void calculatesPaymentAmountCorrectly() {

        Mockito.when(productDao.findProductByCode("B")).thenReturn(new Product("B", new BigDecimal(0.65)));
        Mockito.when(productDao.findProductByCode("M")).thenReturn(new Product("M", new BigDecimal(1.00)));
        Mockito.when(productDao.findProductByCode("C")).thenReturn(new Product("C", new BigDecimal(1.35)));
        Mockito.when(productDao.findProductByCode("W")).thenReturn(new Product("W", new BigDecimal(1.5)));

        List<String> productCodes1 = Arrays.asList("W");
        List<Product> products1 = saleService.generatePurchasableProducts(productCodes1);
        assertEquals(new BigDecimal(1.50), saleService.calculatePayableAmount(products1));

        List<String> productCodes2 = Arrays.asList("B", "C", "W");
        List<Product> products2 = saleService.generatePurchasableProducts(productCodes2);
        assertEquals(new BigDecimal(3.50), saleService.calculatePayableAmount(products2));

    }
}
